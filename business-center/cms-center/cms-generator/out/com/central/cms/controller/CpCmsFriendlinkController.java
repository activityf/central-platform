package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsFriendlink;
import com.central.cms.service.CpCmsFriendlinkService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsFriendlinkController extends BaseController {

    @Autowired
    private CpCmsFriendlinkService cpcmsfriendlinkService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsFriendlink cpcmsfriendlink = cpcmsfriendlinkService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsfriendlink);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsfriendlinkService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsFriendlink cpcmsfriendlink){
        cpcmsfriendlinkService.insertSelective(cpcmsfriendlink);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsFriendlink cpcmsfriendlink){
        cpcmsfriendlinkService.updateSelectiveById(cpcmsfriendlink);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsFriendlink queryBean){
        PageInfo<CpCmsFriendlink> page = cpcmsfriendlinkService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
