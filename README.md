# central-platform


<p align="center"> 
 <img src="https://img.shields.io/circleci/project/vuejs/vue/dev.svg" alt="Build Status">
  <img src="https://img.shields.io/badge/Spring%20Cloud-Finchley.SR2.RELEASE-blue.svg" alt="Coverage Status">
  <img src="https://img.shields.io/badge/Spring%20Boot-2.0.2.RELEASE-blue.svg" alt="Downloads">

</p>

#### 项目介绍
从无到有搭建企业级微服务框架，为企业级项目提供一套完善的，运行稳定，多种分布式问题的解决方案;

central-platform简称CP，基于Spring Cloud(Cloud-Finchley.SR2) 、Spring Boot(2.0.2.RELEASE)
集成layui前后分离的开发平台,其中包括Gateway网关、Oauth认证服务、User用户服务、
Eureka注册中心等多个服务, 为微服务开发所需配置管理、服务发现、断路器、智能路由、
微代理等,努力为企业级打造最全面的微服务开发解决方案;



# 技术介绍  #


<table>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0325/201938_48f25b7d_869801.png "01.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0325/201950_44da7587_869801.png "02.png"/></td>
		<td><img src="https://images.gitee.com/uploads/images/2019/0325/202000_dad634f9_869801.png "03.png"/></td>
    </tr>
</table>

# 技术文档 #
[试读](https://www.kancloud.cn/owenwangwen/open-capacity-platform/content)
[正式文档](https://www.kancloud.cn/owenwangwen/open-capacity-platform) 

 
#项目停止维护更新，请移驾到https://gitee.com/owenwangwen/open-capacity-platform



#### 演示地址

http://59.110.164.254:8066 

账号/密码
admin/admin

#### 安装教程

1.下载代码

```
 git clone https://gitee.com/GeekPerson/central-platform.git
```

2.启动对应的服务

a.先启动 register-center 注册中心的 eureka-server 注册服务

b.在启动 api-gateway 网关服务

c.再启动 oauth-center 认证中心 oauth-server 认证服务

d.在启动 business-center 业务中心的 对应服务 file-center user-center back-center

e.启动 monitor-center 监控中心 admin-server zipkin-center



完整教程可以加群联系开源作者，因一些七牛OSS账号 和 邮箱账号问题；请按照对应的配置进行设置

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/%E6%9C%AA%E6%A0%87%E9%A2%98-1.jpg)

#### 截图预览 

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235123.png)

用户管理

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235514.png)

角色管理

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235606.png)

菜单管理

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235658.png)

权限管理

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235729.png)

应用管理

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235806.png)

token管理

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235841.png)

监控中心

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235915.png)

文档中心

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180827235941.png)

Zipkin监控

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180828001041.png)

文件中心
![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180828001120.png)

个人信息

![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180828001208.png)

任务调度
![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180901233132.png)


![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180901233301.png)


![](https://gitee.com/GeekPerson/central-platform/raw/master/z-doc/images/QQ%E6%88%AA%E5%9B%BE20180901233320.png)

#用户权益
- 允许免费用于学习、毕设、公司项目、私活等。

# 禁止事项 #
- 代码50%以上相似度的二次开源。
- 注意：若禁止条款被发现有权追讨9999的授权费。

### 欢迎进群（群内领资料）

`一键加群`
<a target="_blank" href="https://jq.qq.com/?_wv=1027&k=5JSjd5D"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="open-capacity-platform交流" title="open-capacity-platform交流"></a>        

<table>
	<tr>
            <td><img src="https://images.gitee.com/uploads/images/2019/0330/111355_2a8db09a_869801.png "屏幕截图.png"/></td>
        </tr>
</table>


 