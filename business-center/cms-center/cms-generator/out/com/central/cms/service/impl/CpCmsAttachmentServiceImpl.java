package com.central.cms.service.impl;

import com.central.cms.service.CpCmsAttachmentService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsAttachment;
import com.central.cms.mybatis.mapper.CpCmsAttachmentMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsAttachmentServiceImpl extends BaseServiceImpl<CpCmsAttachmentMapper, CpCmsAttachment> implements CpCmsAttachmentService {


}
