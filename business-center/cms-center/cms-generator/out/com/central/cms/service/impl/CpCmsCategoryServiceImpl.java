package com.central.cms.service.impl;

import com.central.cms.service.CpCmsCategoryService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsCategory;
import com.central.cms.mybatis.mapper.CpCmsCategoryMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsCategoryServiceImpl extends BaseServiceImpl<CpCmsCategoryMapper, CpCmsCategory> implements CpCmsCategoryService {


}
