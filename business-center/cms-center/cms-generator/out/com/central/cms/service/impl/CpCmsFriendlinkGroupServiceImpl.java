package com.central.cms.service.impl;

import com.central.cms.service.CpCmsFriendlinkGroupService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsFriendlinkGroup;
import com.central.cms.mybatis.mapper.CpCmsFriendlinkGroupMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsFriendlinkGroupServiceImpl extends BaseServiceImpl<CpCmsFriendlinkGroupMapper, CpCmsFriendlinkGroup> implements CpCmsFriendlinkGroupService {


}
