package com.central.cms.service.impl;

import com.central.cms.service.CpCmsModelService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsModel;
import com.central.cms.mybatis.mapper.CpCmsModelMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsModelServiceImpl extends BaseServiceImpl<CpCmsModelMapper, CpCmsModel> implements CpCmsModelService {


}
